package com.hendisantika.kotlinhibernatespringsecurity.controller

import com.hendisantika.kotlinhibernatespringsecurity.SiteUser
import com.hendisantika.kotlinhibernatespringsecurity.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

/**
 * Created by IntelliJ IDEA.
 * Project : kotlin-hibernate-spring-security
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-03
 * Time: 19:23
 */
@Controller
@RequestMapping("/register")
class RegisterController(@Autowired private val userService: UserService) {

    @RequestMapping(method = arrayOf(RequestMethod.GET))
    fun doGet(model: Model): String {
        model.addAttribute("user", SiteUser())
        return "register"
    }

    @RequestMapping(method = arrayOf(RequestMethod.POST))
    fun doPost(siteUser: SiteUser): String {
        userService.saveOrUpdate(siteUser)
        return "redirect:/display"
    }
}
