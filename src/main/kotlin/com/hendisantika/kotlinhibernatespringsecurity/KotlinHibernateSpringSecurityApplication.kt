package com.hendisantika.kotlinhibernatespringsecurity

import org.hibernate.SessionFactory
import org.hibernate.annotations.FetchMode
import org.hibernate.annotations.FetchProfile
import org.hibernate.annotations.FetchProfiles
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import javax.persistence.*

@SpringBootApplication
class KotlinHibernateSpringSecurityApplication

@Entity
data class Roles(@field: Id @field: GeneratedValue var id: Int = 0,
                 @field: ManyToOne(targetEntity = SiteUser::class) var user: SiteUser,
                 var role: String = "")

@Entity
data class SiteUser(@field: Id @field: GeneratedValue var id: Int = 0,
                    var userName: String = "",
                    var password: String = "",
                    var enabled: Boolean = true,
                    var accountNonExpired: Boolean = true,
                    var credentialsNonExpired: Boolean = true,
                    var accountNonLocked: Boolean = true,
                    @field: OneToMany(targetEntity = Roles::class) var roles: MutableSet<Roles> = mutableSetOf()) {

    //Convert this class to Spring Security's User object
    fun toUser(): User {
        val authorities = mutableSetOf<GrantedAuthority>()
        roles.forEach { authorities.add(SimpleGrantedAuthority(it.role)) }
        return User(userName, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities)
    }
}

@FetchProfiles(
        FetchProfile(name = "default",
                fetchOverrides = arrayOf(
                        FetchProfile.FetchOverride(entity = SiteUser::class, association = "roles", mode = FetchMode.JOIN)))
)

@Configuration
class DataConfig {
    @Bean
    fun sessionFactory(@Autowired entityManagerFactory: EntityManagerFactory):
            SessionFactory = entityManagerFactory.unwrap(SessionFactory::class.java)
}

fun main(args: Array<String>) {
    runApplication<KotlinHibernateSpringSecurityApplication>(*args)
}
